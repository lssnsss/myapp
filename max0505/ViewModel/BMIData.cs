﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace max0505.ViewModel
{
    public class BMIData
    {
        public float? Height { get; set; }
        public float? Weight { get; set; }
        public float? Result { get; set; }
        public String Level { get; set; }
    }
}