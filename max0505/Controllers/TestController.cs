﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace max0505.Controllers
{
    public class TestController : Controller
    {
        // GET: Test
        public ActionResult Index()
        {
            ViewData["KeyName"] = "111";
            ViewBag.Name = "bad";

            return View();
        }
        public ActionResult Html()
        {
            return View();
        }
        public ActionResult HtmlHelper()
        {
            return View();
        }
        public ActionResult Razor()
        {
            return View();
        }
    }
}